#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void create_ships(int argc, char *argv[]);
int place_ships(char boat[6], int *p, int *z, char **board, int *x, int *y);
int check_board(char boat[6], int *p, int *z, char **board, int newdirec, int *x, int *y);
void fire_round(char **board, long int *a, long int *b, int *x, int *y);
void save_board(char **board, const char *name, int *x, int *y);
int ships_left(const char *name);
