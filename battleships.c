#include "battleships.h"

int main(int argc, char *argv[]){

	create_ships(argc, argv);
}

void create_ships(int argc, char *argv[]){

	int z = 0, p = 0, j = 0, t = 0, q = 0, r = 0, x = 0, y = 0;
	int newlinetracker = 0;
	long int a, b;
	char boat[6] = {'\0', '\0', '\0', '\0', '\0', '\0'};
	char ch;
	const char *name = "gameboard";

	if (argc != 5){
		printf("Error 1\n");
		exit(0);		
	}

	y = strtol(argv[1], NULL, 10);

	if (y < 10){
		printf("Error 2\n");
		exit(0);
	}

	x = strtol(argv[2], NULL, 10);

	if (x < 10){
		printf("Error 3\n");
		exit(0);
	}

	a = strtol(argv[3], NULL, 10);

	if (a < 0 || a >= y){
		printf("Error 4\n");
		exit(0);
	}

	b = strtol(argv[4], NULL, 10);
	
	if (b < 0 || b >= x){
		printf("Error 5\n");
		exit(0);
	}	

	FILE *fp;

	fp = fopen(name, "r+");

	char **board;
	if (!fp){
		board = malloc(y * sizeof(char *));
		for(; j < y; j++){
			board[j] = malloc(x * sizeof(char ));
		}

		for (; q < y; q++){
			memset(board[q], '*', x);
		}
	}
	else{
		fseek(fp, 0, SEEK_SET);
		while((ch = fgetc(fp)) != EOF){
			if (ch == '\n'){
				x = ftell(fp)-1;
				break;
			}
		}
		fseek(fp, 0, SEEK_SET);
		while((ch = fgetc(fp)) != EOF){
			if (ch == '\n'){
				newlinetracker++;
			}
		}
		y = newlinetracker;	

		board = malloc(y * sizeof(char *));
		for(j = 0; j < y; j++){
			board[j] = malloc(x * sizeof(char ));
		}

		fseek(fp, 0, SEEK_SET);
		for(j = 0; j < y; j++){
			for(r = 0; r < x; r++){
				board[j][r] = fgetc(fp);
			}
			fgetc(fp);
		}
		
		goto playgame;
		
	}

	while(1){
		srand(time(NULL));
		do {
			z = rand() % 10;
			p = rand() % 10;
			while(board[z][p] != '*'){			
				z = rand() % 10;
				p = rand() % 10;
			}
			boat[0] = 'A';
			boat[1] = 'A';
			boat[2] = 'A';
			boat[3] = 'A';
			boat[4] = 'A';
			boat[5] = '\0';
		}while (place_ships(boat, &p, &z, board, &x, &y) == -1);
		do {
			z = rand() % 10;
			p = rand() % 10;
			while(board[z][p] != '*'){			
				z = rand() % 10;
				p = rand() % 10;
			}
			boat[0] = 'B';
			boat[1] = 'B';
			boat[2] = 'B';
			boat[3] = 'B';
			boat[4] = '\0';
		}while (place_ships(boat, &p, &z, board, &x, &y) == -1);
		do {
			z = rand() % 10;
			p = rand() % 10;
			while(board[z][p] != '*'){			
				z = rand() % 10;
				p = rand() % 10;
			}
			boat[0] = 'D';
			boat[1] = 'D';
			boat[2] = 'D';
			boat[3] = '\0';
		}while (place_ships(boat, &p, &z, board, &x, &y) == -1);
		do {
			z = rand() % 10;
			p = rand() % 10;
			while(board[z][p] != '*'){			
				z = rand() % 10;
				p = rand() % 10;
			}
			boat[0] = 'C';
			boat[1] = 'C';
			boat[2] = 'C';
			boat[3] = '\0';
		}while (place_ships(boat, &p, &z, board, &x, &y) == -1);
		do {
			z = rand() % 10;
			p = rand() % 10;
			while(board[z][p] != '*'){			
				z = rand() % 10;
				p = rand() % 10;
			}
			boat[0] = 'S';
			boat[1] = 'S';
			boat[2] = '\0';
		}while (place_ships(boat, &p, &z, board, &x, &y) == -1);
		do {
			z = rand() % 10;
			p = rand() % 10;
			while(board[z][p] != '*'){			
				z = rand() % 10;
				p = rand() % 10;
			}
			boat[0] = 'P';
			boat[1] = 'P';
			boat[2] = '\0';
		}while (place_ships(boat, &p, &z, board, &x, &y) == -1);
		break;
	}

	playgame:

		fire_round(board, &a, &b, &x, &y);
	
		save_board(board, name, &x, &y);

		ships_left(name);

		for(t = 0; t < y; t++){
			free(board[t]);
		}
		free(board);
}

int place_ships(char boat[6], int *p, int *z, char **board, int *x, int *y){

	int newdirec = 0, i = 0;

	newdirec = rand() % 4;
	
	switch (newdirec){
		
		case 0:
			if (*z + (int)strlen(boat) < *y){
				if (check_board(boat, p, z, board, newdirec, x, y) == -1){
					return (-1);
				}

				for (i = 0; i < (int)strlen(boat); i++){
					board[(*z)--][*p] = boat[i];
				}
				break;
			}
			else{
				return(-1);
			}
		case 1:
			if (*z - (int)strlen(boat) > 0){
				if (check_board(boat, p, z, board, newdirec, x, y) == -1){
					return (-1);
				}
				for (i = 0; i < (int)strlen(boat); i++){
					board[(*z)++][*p] = boat[i];
				}
				break;
			}
			else{
				return(-1);
			}
		case 2:
			if (*p + (int)strlen(boat) < *x){
				if (check_board(boat, p, z, board, newdirec, x, y) == -1){
					return (-1);
				}
				for (i = 0; i < (int)strlen(boat); i++){
					board[*z][(*p)++] = boat[i];
				}
				break;
			}
			else{
				return(-1);
			}
		case 3:
			if(*p - (int)strlen(boat) > 0){
				if (check_board(boat, p, z, board, newdirec, x, y) == -1){
					return (-1);
				}
				for (i = 0; i < (int)strlen(boat); i++){
					board[*z][(*p)--] = boat[i];
				}
				break;
			}
			else{
				return(-1);
			}		
	}
	return (1);
}

int check_board(char boat[6], int *p, int *z, char **board, int newdirec, int *x, int *y){

	int i = 0, j = 0, tempy = 0, tempx = 0, endx = 0, endy = 0;

	tempy = *z;
	tempx = *p;
	endx = tempx;
	endy = tempy;

	switch (newdirec){
		
		case 0:
			tempy -= (int)strlen(boat);
			break;
		case 1:
			endy += (int)strlen(boat);
			break;
		case 2:
			endx += (int)strlen(boat);
			break;
		case 3:
			tempx -= (int)strlen(boat);
			break;
	}

	if (endy + (int)strlen(boat) > *y || endx + (int)strlen(boat) > *x || endy - (int)strlen(boat) < 0 || endx - (int)strlen(boat) < 0){
		return(-1);
	}

	for (i = tempy; i <= endy; i++){
		for (j = tempx; j <= endx; j++){
			if (board[i][j] != '*'){
				return(-1);
			}
		}
	}
	return(1);
}

void fire_round(char **board, long int *a, long int *b, int *x, int *y){

	int c = 0, d = 0, v = 0, r = 0;
	char star = '*';

	c = *a;
	d = *b;

	if (board[c][d] != '*'){
		printf("HIT!\n");
		board[c][d] = 'X';
	}
	else{
		printf("MISS!\n");	
		board[c][d] = 'M';
	}

	for (v = 0; v < *y; v++){
		for (r = 0; r < *x; r++){
			if (board[v][r] == 'X' || board[v][r] == 'M'){
				printf("%c", board[v][r]);
			}
			else{
				printf("%c", star);
			}
		}
		printf("\n");
	}

}

void save_board(char **board, const char *name, int *x, int *y){

	int a = 0, b = 0;

	FILE *fp;

	fp = fopen(name, "r+");

	if (!fp){
		fp = fopen(name, "w");
		for (a = 0; a < *y; a++){
			for (b = 0; b < *x; b++){
				fprintf(fp, "%c", board[a][b]);
			}
			fprintf(fp, "\n");
		}
	}

	fseek(fp, 0, SEEK_SET);

	for (int i = 0; i < *y; i++){
		for (int z = 0; z < *x; z++){
			if (fgetc(fp) == 'X' && board[i][z] != 'X'){
				;
			}
			else{
				fseek(fp, -1, SEEK_CUR);
				fputc(board[i][z], fp);
			}
		}	
		fprintf(fp, "\n");
	}	

	fclose(fp);
}

int ships_left(const char *name){

	int pt = 0, sub = 0, cru = 0, des = 0, bat = 0, air = 0, totalships = 0;
	char ch;

	FILE *fp;

	fp = fopen(name, "r+");

	if (!fp){
		printf("Unable to open file.\n");
		return(-1);
	}

	fseek(fp, 0, SEEK_SET);

	while((ch = fgetc(fp)) != EOF){
		if (ch == 'P'){
			pt = 1;
		}
		else if (ch == 'S'){
			sub = 1;
		}
		else if (ch == 'C'){
			cru = 1;
		}
		else if (ch == 'D'){
			des = 1;
		}
		else if (ch == 'B'){
			bat = 1;
		}
		else if (ch == 'A'){
			air = 1;
		}
	}

	totalships = pt+sub+cru+des+bat+air;
	printf("Total number of ships left: %d\n", totalships);	

	fclose(fp);
	return(1);
}





